let inventory = require('./CarJsFile')

// Here i used for loop and If condition to get result

let n = 33;

function problem1(inventory,n){
    if (inventory === undefined){
        return []
    }
    else if (Array.isArray(inventory) !== true){
        return []
    }
    else if (typeof(n) !== "number"){
        return []
    }
    else if (inventory.length === 0){
        return []
    }
    else if (n === undefined){
        return []
    }
    else{
        for (let i=0; i<inventory.length; i+=1){
            if (inventory[i].id === n){
                return inventory[i];
            };
        };
    };
};

const result = problem1(inventory,n);

console.log(result)

module.exports = problem1;
